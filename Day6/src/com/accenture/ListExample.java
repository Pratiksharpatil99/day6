package com.accenture;

import java.util.LinkedHashSet;
import java.util.Set;

public class ListExample {
	public static void main(String[] args) {
		
	Set<String> data = new LinkedHashSet<String>();   
    
    data.add("JavaTpoint");   
    data.add("Set");   
    data.add("Example");  
    data.add("English");
    data.add("Set");   

    System.out.println(data);   
}
}